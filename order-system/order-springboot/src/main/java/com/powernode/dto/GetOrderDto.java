package com.powernode.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 描述: 封装前端搜索订单的实体类
 * 作者 张云博
 * 时间 2024/2/20 19:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
public class GetOrderDto extends BaseDto{

    /**
     * 订单编号
     */
    private String orderNum;


    /**
     * 支付方式  1:支付宝 2:微信 3:货到付款
     */
    private String payType;

}
