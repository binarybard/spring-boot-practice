package com.powernode.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

/**
 * 描述: 封装修改订单数据的实体类
 * 作者 张云博
 * 时间 2024/2/20 19:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
public class EditOrderDto {

    /**
     * 主键
     */
    @NotNull(message = "主键id不能为空")
    private Integer id;

    /**
     * 订单编号
     */
    @Length(min = 16, max = 16, message = "字符串长度必须为16,年月日+8位随机数")
    @NotNull(message = "订单编号不能为空")
    private String orderNum;

    /**
     * 收货人
     */
    @Pattern(regexp = "[\\u4e00-\\u9fa5]{2,20}",message = "用户名是2-20位中文")
    @NotNull(message = "收货人不能为空")
    private String uName;

    /**
     * 手机号
     */
    @Pattern(regexp = "^1[358]\\d{9}$", message = "手机号格式要按要求")
    @NotNull(message = "手机号不能为空")
    private String tel;

    /**
     * 总金额
     */
    @NotNull(message = "总金额不能为空")
    private Double total;

    /**
     * 应付金额
     */
    @NotNull(message = "应付金额不能为空")
    private Double money;

    /**
     * 订单状态 1:待确认 2:已经确认 3:已经收货
     */
    @NotNull(message = "订单状态不能为空")
    private String status;

    /**
     * 支付状态 1:已经支付 0:没有支付
     */
    @NotNull(message = "支付状态不能为空")
    private String payStatus;

    /**
     * 发货状态 1:已经发货 0:没有发货
     */
    @NotNull(message = "发货状态不能为空")
    private String expressStatus;

    /**
     * 支付方式  1:支付宝 2:微信 3:货到付款
     */
    @NotNull(message = "支付方式不能为空")
    private String payType;

    /**
     * 配送方式  1:顺丰快递  2:申通快递 3:韵达快递
     */
    @NotNull(message = "配送方式不能为空")
    private String expressType;

    /**
     * 下单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS", timezone = "GMT+08")
    @NotNull(message = "下单时间不能为空")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:SS") // 避免前端显示时间戳
    private Date createTime;

}
