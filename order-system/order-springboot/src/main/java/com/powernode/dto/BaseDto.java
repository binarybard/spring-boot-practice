package com.powernode.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.validation.constraints.Size;

/**
 * 描述: 基础的DTO 分页数据
 * 作者 张云博
 * 时间 2024/2/20 14:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class BaseDto {

    // 页码
    @Size(max = 9999,min = 1,message = "非法页码")
    public Integer pageNum = 1;

    // 页面数据条数
    @Size(max = 999,min = 1,message = "非法页面数据条数")
    public Integer pageSize = 10;
}



