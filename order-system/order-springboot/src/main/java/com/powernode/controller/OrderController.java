package com.powernode.controller;

import com.powernode.common.Result;
import com.powernode.dto.AddOrderDto;
import com.powernode.dto.EditOrderDto;
import com.powernode.dto.GetOrderDto;
import com.powernode.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 描述: 订单系统的控制层
 * 作者 张云博
 * 时间 2024/2/20 20:21
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 分页 + 条件查询
     * @param record
     * @return
     */
    @GetMapping("getOrders")
    public Result getOrders(GetOrderDto record){
        return orderService.findOrders(record);
    }


    /**
     * 新增订单
     * @param record
     * @return
     */
    @PostMapping("addOrder")
    public Result addOrder(AddOrderDto record){
        return orderService.saveOrder(record);
    }

    /**
     * 修改订单
     * @param record
     * @return
     */
    @PutMapping("editOrder")
    public Result editOrder(EditOrderDto record){
        return orderService.modifyOrder(record);
    }


    /**
     * 删除订单
     * @param idList
     * @return
     */
    @DeleteMapping("cutOrders")
    public Result cutOrders(@RequestParam List<Integer> idList){
        return orderService.removeOrder(idList);
    }







}
