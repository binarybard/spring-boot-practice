package com.powernode.controller;

import com.powernode.common.Result;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * 描述: 全局异常处理器
 * 作者 张云博
 * 时间 2024/2/20 20:01
 */
@RestControllerAdvice
public class ExceptionController {

    // 捕获参数的异常
    @ExceptionHandler(BindException.class)
    public Result handleBindException(BindException e){
        List<ObjectError> allErrors = e.getAllErrors();
        StringBuilder sb = new StringBuilder();
        // 拼接所有的错误信息
        for (ObjectError error : allErrors) {
            String message = error.getDefaultMessage();
            sb.append(message);
            sb.append(",");
        }
        return new Result(-1,sb.toString());
    }



    // 捕获所有的异常
    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e){
        e.printStackTrace();
        return Result.SYSTEM_ERROR; // 后台接口异常
    }

}
