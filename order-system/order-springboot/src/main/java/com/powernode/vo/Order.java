package com.powernode.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;


/**
 * 订单表
 */
@Data
public class Order {
    /**
    * 主键
    */
    private Integer id;

    /**
    * 订单编号
    */
    private String orderNum;

    /**
    * 收货人
    */
    @JsonProperty("uName")
    private String uName;

    /**
    * 手机号
    */
    private String tel;

    /**
    * 总金额
    */
    private Double total;

    /**
    * 应付金额
    */
    private Double money;

    /**
    * 订单状态 1:待确认 2:已经确认 3:已经收货
    */
    private String status;

    /**
    * 支付状态 1:已经支付 0:没有支付
    */
    private String payStatus;

    /**
    * 发货状态 1:已经发货 0:没有发货
    */
    private String expressStatus;

    /**
    * 支付方式  1:支付宝 2:微信 3:货到付款
    */
    private String payType;

    /**
    * 配送方式  1:顺丰快递  2:申通快递 3:韵达快递
    */
    private String expressType;

    /**
     * 下单时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:SS", timezone = "GMT+08")
    private Date createTime;
}