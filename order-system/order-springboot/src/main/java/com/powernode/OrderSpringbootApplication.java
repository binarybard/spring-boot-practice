package com.powernode;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
// 扫描持久层,加入ioc容器中
@MapperScan(value = {"com.powernode.mapper"})
public class OrderSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderSpringbootApplication.class, args);
    }

}
