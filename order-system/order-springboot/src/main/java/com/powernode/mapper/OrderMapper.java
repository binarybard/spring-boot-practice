package com.powernode.mapper;

import com.powernode.dto.AddOrderDto;
import com.powernode.dto.EditOrderDto;
import com.powernode.dto.GetOrderDto;
import com.powernode.vo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* 描述: 操作订单表的持久层
* 作者 张云博
* 时间 2024/2/20 19:23
*/
public interface OrderMapper {

    // 根据id删除订单
    int deleteByPrimaryKey(Integer id);

    // 新增订单
    int insertSelective(AddOrderDto record);

    // 修改订单
    int updateByPrimaryKeySelective(EditOrderDto record);

    // 分页和条件查询
    List<Order> selectOrderByCondition(GetOrderDto record);

    // 批量删除
    int deleteBatchByIdList(@Param("idList") List<Integer> idList);




    int insert(Order record);

    Order selectByPrimaryKey(Integer id);


    int updateByPrimaryKey(Order record);
}