package com.powernode.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.powernode.dto.BaseDto;

/**
 * 描述: 基础的业务类  存放常用业务的方法
 * 作者 张云博
 * 时间 2024/2/20 14:43
 */
public class BaseServiceImpl {


    // 开启分页
    public Page<Object> startPage(BaseDto record){
        return PageHelper.startPage(record.getPageNum(),record.getPageSize());
    }

}
