package com.powernode.service;

import com.powernode.common.Result;
import com.powernode.dto.AddOrderDto;
import com.powernode.dto.EditOrderDto;
import com.powernode.dto.GetOrderDto;

import java.util.List;

/**
 * 描述: 订单系统业务层接口
 * 作者 张云博
 * 时间 2024/2/20 20:07
 */
public interface OrderService {

    /**
     * 新增订单
     * @param record
     * @return
     */
    Result saveOrder(AddOrderDto record);

    /**
     * 修改订单
     * @param record
     * @return
     */
    Result modifyOrder(EditOrderDto record);

    /**
     * 分页 + 条件 查询订单
     * @param record
     * @return
     */
    Result findOrders(GetOrderDto record);


    /**
     * 删除订单
     * @param idList
     * @return
     */
    Result removeOrder(List<Integer> idList);


}
