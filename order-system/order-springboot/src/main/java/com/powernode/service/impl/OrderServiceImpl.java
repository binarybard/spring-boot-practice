package com.powernode.service.impl;

import com.github.pagehelper.Page;
import com.powernode.common.Result;
import com.powernode.dto.AddOrderDto;
import com.powernode.dto.EditOrderDto;
import com.powernode.dto.GetOrderDto;
import com.powernode.mapper.OrderMapper;
import com.powernode.service.OrderService;
import com.powernode.vo.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述: 订单系统业务层接口实现类
 * 作者 张云博
 * 时间 2024/2/20 20:10
 */
@Service
public class OrderServiceImpl extends BaseServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 新增订单
     * @param record
     * @return
     */
    @Override
    public Result saveOrder(AddOrderDto record) {
        int i = orderMapper.insertSelective(record);
        return i > 0 ? Result.ADD_ONE_RECORD_SUCCESS : Result.ADD_ONE_RECORD_FAIL;
    }

    /**
     * 修改订单
     * @param record
     * @return
     */
    @Override
    public Result modifyOrder(EditOrderDto record) {
        int i = orderMapper.updateByPrimaryKeySelective(record);
        return i > 0 ? Result.EDIT_ONE_RECORD_SUCCESS : Result.EDIT_ONE_RECORD_FAIL;
    }


    /**
     * 分页 + 条件 查询订单
     * @param record
     * @return
     */
    @Override
    public Result findOrders(GetOrderDto record) {
        // 开启分页
        Page<Object> page = startPage(record);
        System.out.println(record);
        // 查询
        List<Order> orderList = orderMapper.selectOrderByCondition(record);
        return new Result(page.getTotal(),orderList);
    }



    /**
     * 删除订单  处理删除一个订单 和 批量删除
     * @param idList
     * @return
     */
    @Override
    public Result removeOrder(List<Integer> idList) {
        int i = orderMapper.deleteBatchByIdList(idList);
        // 如果影响行数是1,就是删除一个数据
        if (i == 1){
            return Result.CUT_ONE_RECORD_SUCCESS;
        }else if(i <= 0){
            // 删除操作失败
            return Result.CUT_OPERATE_FAIL;
        }
        // 批量删除成功
        return Result.CUT_BATCH_RECORD_SUCCESS;
    }
}
