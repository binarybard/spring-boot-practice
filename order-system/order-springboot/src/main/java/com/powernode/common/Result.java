package com.powernode.common;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 描述: 统一结果集封装
 */
@Data
@NoArgsConstructor
public class Result{
    /**
     * 业务逻辑状态码
     *  2000: 查询成功
     *  886: 登录过期
     *  200: 添加/删除/修改操作成功
     *  -1:  添加/删除/修改操作失败
     */
    private Integer code = 200;
    /**
     * 操作通知消息
     */
    private String msg = "操作成功";
    /**
     * 满足sql查询条件的总记录数
     */
    private Long total = 0L;
    /**
     * 查询数据
     */
    private Object data;

    /*公共通用模块*/
    // 增删改成功的结果
    public static final Result ADD_ONE_RECORD_SUCCESS = new Result(200,"成功添加一条记录");
    public static final Result EDIT_ONE_RECORD_SUCCESS = new Result(200,"成功修改一条记录");
    public static final Result CUT_ONE_RECORD_SUCCESS = new Result(200,"成功删除一条记录");
    public static final Result CUT_BATCH_RECORD_SUCCESS = new Result(200,"成功删除一批记录");

    // 增删改失败的结果
    public static final Result ADD_ONE_RECORD_FAIL = new Result(-1,"添加一条记录失败");
    public static final Result EDIT_ONE_RECORD_FAIL = new Result(-1,"修改一条记录失败");
    public static final Result CUT_OPERATE_FAIL = new Result(-1,"删除操作失败");

    // 登录相关
    public static final Result LOGIN_STATUS_INVALID = new Result(886,"当前用户和会话已过期");
    public static final Result LOGOUT = new Result(200,"退出登录");

    // 后端异常结果
    public static final Result SYSTEM_ERROR = new Result(-1,"后台接口请求异常");
    public static final Result DATA_FORMAT_ERROR = new Result(-1,"数据格式错误");



    /**
     * 给修改【增、删、改】操作用
     * @param code
     * @param msg
     */
    public Result(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    /**
     * 给查询操作使用
     * @param total
     * @param data
     */
    public Result(long total, Object data){
        this.code = 2000;
        this.total = total;
        this.data = data;
    }

}
