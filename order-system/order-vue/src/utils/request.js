/*引入axios*/
import axios from "axios";
/*从element ui 中引入消息组件*/
import {Message} from "element-ui"

/*创建axios 不完整 实例*/
let instance = axios.create({
    baseURL:"/api", // 基础路径
    timeout:7000, // 超时时间
});

/**
 * 给axios 配置前置请求拦截器
 * 1.axios 请求如果是正常的发送出去,则执行第一个方法
 * 2.axios 请求在发送时出了错误,则执行第二个方法
 */
instance.interceptors.request.use(function (config) {
     return config // 代表放行请求
},function (error){
    // 将错误抛给axios 的 catch
    return Promise.reject("请求被拦截");
})


/**
 * 给axios 实例配置后置响应拦截
 * 方法1:如果后端接口是一个正常的响应,则执行第一个方法 then
 * 方法2:如果后端接口异常,则执行第二个方法 catch
 *
 */
instance.interceptors.response.use(function (result) {

    // 获取接口返回的数据
    let data = result.data;
    //console.log(data);
    // 判断code的状态
    switch (data.code) {
        case 886:
            location.href = "/#/login";
            break;
        case 2000: // 查询成功的响应码
            return data;
            break;
        case 200: // 增删改成功的响应码
            Message.success("操作成功");
            return data;
            break;
        case -1:
            Message.error(data.msg);
            return Promise.reject(); // 走catch,catch里面没有内容,所有就只有错误提醒
            break;
    }

}, function (error) {
    Message.error("后置拦截,系统服务器异常"); // 等价上面的方法
})


/*
    1.资源路径
    2.请求方式
    3.请求参数
 */
// 返回的是一个完整的axios 实例
let request = function (requestConfig){
    return instance(requestConfig);
};

export default request