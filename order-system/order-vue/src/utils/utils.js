export default {
    /*生成8位随机订单号,拼接成16位*/
    generateOrderNum() {
        let d = new Date();
        let s = d.toLocaleDateString().split("/");
        return `${s[0]}${s[1].padStart(2, '0')}${s[2]}${Math.floor(10000000 + Math.random() * 90000000)}`;
    }
}