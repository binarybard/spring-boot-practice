/*引入request*/
import request from "@/utils/request";

/*封装和订单相关请求的api*/

let orderApi = {

    /*查询接口*/
    getOrders(params){
        return request({
            url:'/order/getOrders',
            method:'GET',
            params
        })
    },


    /*新增订单*/
    addOrder(params){
        return request({
            url:'/order/addOrder',
            method:'POST',
            params
        })
    },

    /*修改订单*/
    editOrder(params){
        return request({
            url:'/order/editOrder',
            method:'PUT',
            params
        })
    },


    /*(批量)删除订单*/
    cutOrders(params){
        return request({
            url:'/order/cutOrders' + params,
            method:'DELETE',
        })
    }


}

export default orderApi









