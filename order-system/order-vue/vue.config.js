const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // 前端解决跨域问题
  devServer: {
    proxy: {
      '/api': {
        target: 'http://localhost:8080',
        // 允许跨域
        changeOrigin: true,
        ws: true,
        //移除url里面的api
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
})
